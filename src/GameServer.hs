{-# LANGUAGE TupleSections #-}
module GameServer where
import Control.Concurrent ( forkIO, ThreadId, threadDelay, yield )
import Network.Socket
  ( Family (AF_INET),
    SockAddr (SockAddrInet),
    SocketType (Stream),
    SocketOption (ReuseAddr),
    bind, socket, listen, accept, setSocketOption, Socket
  )
import GameClient (gamePort)
import Control.Monad (void, when)
import Text.Printf (printf)
import Control.Concurrent.STM (TChan, newTChan, atomically, readTChan)
import GameUtils
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import qualified Data.Map  as M(Map, delete, toList, insert, (!), empty, (!?))
import qualified Network.Socket.ByteString.Lazy as B
import Data.Binary (encode)
import Control.Concurrent.STM.TChan (isEmptyTChan)
import Data.Bifunctor (Bifunctor(bimap, second))
import Data.Int (Int32)
import Generation (RandomSeed, generateFieldAt, uniqueMatIndex)
import qualified Data.ByteString.Lazy as B

createHandler :: PlayerId -> Conns -> Socket -> SockAddr -> IO ()
createHandler playerId ioconns sock addr = do
  printf "connected to %s\n" $ show addr
  conns <- readIORef ioconns
  let PlayerConnection{ pendingEvents = conn } = conns M.! playerId
  handleGameEvent sock conn
  printf "connection to %s lost\n" $ show addr
  conns' <- readIORef ioconns
  let newConns = M.delete playerId conns'
  writeIORef ioconns newConns

data PlayerConnection = PlayerConnection {
  connection :: (Socket, SockAddr),
  pendingEvents :: TChan GameEvent
  }

type Conns = IORef (M.Map PlayerId PlayerConnection)

instance Show PlayerConnection where
  show PlayerConnection {connection = c, pendingEvents = _} = "connection { " ++ show c ++ "}"

data ServerEntity = ServerEntity {
  getLastMoveTime :: Time,
  getEntity :: Entity
  } deriving Show

-- onEntity :: (forall e. IsEntity e => e -> t) -> ServerEntity -> t
-- onEntity g a = g (getEntity a)

-- onEntityM :: (forall e. IsEntity e => e -> e) -> ServerEntity -> ServerEntity
-- onEntityM g a = a{getEntity = g (getEntity a)}

instance IsEntity ServerEntity where
    getEntityImpl ent = convertEntityImpl (getEntity ent) (\x -> ent {getEntity = x})
    getEntityType ent = getEntityType $ getEntity ent
    getInner = getEntity

data GameState = GameState {
  entities :: M.Map PlayerId ServerEntity,
  lastTickTime :: Time,
  getSeeDist :: Int,
  generatedMap :: M.Map Int FieldType
  }
type PlayerId = Int

data Destination = Broadcast
                 | PlayerDest PlayerId
  deriving Show

type Event = (Destination, GameEvent)

data Field = Field (Int, Int) FieldType deriving Show

defaultState :: IO GameState
defaultState = do
  time <- getTime
  return GameState { entities = M.empty,
                     lastTickTime = time,
                     getSeeDist = 4,
                     generatedMap = M.empty
                     }

seenFields :: GameState -> RandomSeed -> Int -> Int -> Int -> (GameState, [Field])
seenFields state seed x y dist = let xs = [let i = uniqueMatIndex a b
                                               f = generateFieldAt seed i
                                               ff s = M.insert i f $ generatedMap s
                                           in
                                             \s -> (Field (a,b) f, s {generatedMap = ff s}) |
                                           a <- [(x-dist)..(x+dist)],
                                           b <- [(y-dist)..(y+dist)],
                                           abs (a-x) + abs (b-y) <= dist]
  in let a st [] = (st, [])
         a st (z:zs) = let (v, ns) = z st in
           second (v:) $ a ns zs
     in a state xs

fieldToEvent :: Field -> GameEvent
fieldToEvent (Field pos typ) = ExploreField (bimap fromIntegral fromIntegral pos) typ

emptyPlayer :: Int -> Player
emptyPlayer pid = Player {
  getPlayerId = pid,
  playerPosition = (0, 0),
  getPlayerDirection = DDown,
  getPlayerHealth = getMaxHealth (undefined :: Player),
  getPlayerName = "ServerPlayer"
  }

moveTimeout :: Time
moveTimeout = 50

calcDamage :: (IsEntity a, IsEntity b) => a -> b -> Int
calcDamage _att _tar = 1

processClientEvents :: RandomSeed -> Time -> (PlayerId, Time, [GameEvent]) -> (GameState, [Event]) -> (GameState, [Event])
processClientEvents _ _ (_, _, []) x = x
processClientEvents seed time (pid, dt, e:es) (state, emittedEvents) =
  let (newState, extraEvents) =
        case e of
          Connect -> let newPlayer@Player{playerPosition = pos} = emptyPlayer pid
                         (newstate, sf) = seenFields state seed 0 0 (getSeeDist state)
                         rpos = bimap fromIntegral fromIntegral pos
                     in
                       (newstate { entities = M.insert pid (ServerEntity time (Entity newPlayer)) $ entities state },
                        map ((PlayerDest pid,) . fieldToEvent) sf ++
                        [(PlayerDest pid, uncurry (Join (fromIntegral pid)) rpos),
                         (PlayerDest pid, (uncurry YourPosition) rpos dt $ getDirection newPlayer)])
          MoveRequest dx dy -> let ServerEntity lastMove ent = entities state M.! pid
                                   (cx, cy) = getPosition ent
                                   dir  = if dx > 0 then if dy == 0 then DRight else undefined
                                     else if dx < 0 then if dy == 0 then DLeft else undefined
                                     else if dy > 0 then DDown else DUp
                                   nx = cx + fromIntegral dx
                                   ny = cy + fromIntegral dy
                                   ent' = setPosition  ent (nx, ny) dir
                                   
                                   newPlayer = ServerEntity time ent'
                                   newPlayers = M.insert pid newPlayer $ entities state
                                   (newstate, sf) = seenFields state seed (fromIntegral nx) (fromIntegral ny) (getSeeDist state)
                               in if time - lastMove <= moveTimeout then (state, []) else
            (newstate{entities = newPlayers}, map ((PlayerDest pid,) . fieldToEvent) sf
              ++ [(PlayerDest pid, YourPosition (fromIntegral nx) (fromIntegral ny) (max dt moveTimeout) dir)])
          Attack eid -> let attacker = entities state M.!? pid
                            target = entities state M.!? fromIntegral eid
                            in maybe (state, []) (\t -> 
                               let damage = maybe 0 (\a -> calcDamage a t) attacker
                                   currentHealth = getHealth t
                                   newHealth = max (currentHealth - damage) 0
                                   newTarget = setHealth t newHealth
                                   newTable = M.insert (fromIntegral eid) newTarget $ entities state 
                                   damage_ev = (PlayerDest (fromIntegral eid), Health eid (fromIntegral newHealth)) in
                                 (state {entities = newTable}, case newHealth of
                                           0 -> [damage_ev, (Broadcast, Death eid)]
                                           _ -> [damage_ev])
                            ) target
          _ -> error $ "[err] uninmplemented event: " ++ show e
  in processClientEvents seed time (pid, dt, es) (newState, emittedEvents ++ extraEvents)

processTick :: Time -> GameState -> (GameState, [Event])
processTick _dt state = (state, [])

emmitEvents :: [Event] -> Conns -> IO ()
emmitEvents [] _ = return ()
emmitEvents ((dest, event):xs) ioconns = do
  conns <- readIORef ioconns
  let socks = case dest of
        Broadcast -> map (\(_, PlayerConnection {connection = (s, _)}) -> s) $ M.toList conns
        PlayerDest pid -> [fst $ connection $ conns M.! pid]
  mapM_ (\sock -> case event of
           YourPosition x y dt dir -> do
             _ <- B.send sock $ B.concat [encode eventYourPosition, -- Position
                                          encode x, -- x
                                          encode y, -- y
                                          encode dt, -- dt
                                          encode $ direction2Int dir] -- direction
             return ()
           ExploreField (x, y) f -> do
             _ <- B.send sock $ B.concat [encode eventExploreField, -- Position
                                          encode x, -- x
                                          encode y, -- y
                                          encode (fieldType2Int f :: Int32)] -- y
             return ()
           Health eid health -> do
             _ <- B.send sock $ B.concat [encode eventHealth, -- Position
                                          encode eid,
                                          encode health]
             return ()
           Death eid -> do
             _ <- B.send sock $ B.concat [encode eventDeath, -- Position
                                          encode eid]
             return ()
           Join pid x y -> do
             _ <- B.send sock $ B.concat [encode eventJoin, -- Position
                                          encode pid,
                                          encode x,
                                          encode y]
             return ()
           Log str -> putStrLn $ "[log] " ++ str
           x -> error $ "TODO: emmit event: " ++ show x
        ) socks

  emmitEvents xs ioconns

tickLoop :: RandomSeed -> Conns -> GameState -> IO ()
tickLoop seed ioconns state = do
  -- skip if no players connected
  conns <- readIORef ioconns
  when (null conns) $ yield >> tickLoop seed ioconns state
  time <- getTime

  let lastTime = lastTickTime state
  let state' = state {lastTickTime = time}
  let dt = time - lastTime

  chans <- map (\(k, PlayerConnection{pendingEvents = e}) -> (k, e)) . M.toList <$> readIORef ioconns

  eventList <- mapM (\(pid, chan) -> do
            let f es = do
                  isEmpty <- atomically $ isEmptyTChan chan
                  if isEmpty then return es else do
                    event <- atomically $ readTChan chan
                    f (event:es)
            (pid,dt,) <$> f []
          ) chans

  time' <- getTime
  let (newState', events') = foldr (processClientEvents seed time') (state', []) eventList
  let (newState, events) = processTick dt newState'
  emmitEvents (events' ++ events) ioconns

  yield
  threadDelay (20 * 100)
  tickLoop seed ioconns newState

startServer :: IO ThreadId
startServer = do
  putStrLn "starting server..."
  sock <- socket AF_INET Stream 0
  setSocketOption sock ReuseAddr 1
  bind sock (SockAddrInet gamePort 0)
  listen sock 5

  seed <- fromIntegral <$> getTime

  conns <- newIORef M.empty
  void $ defaultState >>= forkIO . tickLoop seed conns

  let connectionWaitLoop playerId = do
        putStrLn "waiting for connection..."
        c@(conn, addr) <- accept sock

        oldConns <- readIORef conns
        chan <- atomically newTChan

        writeIORef conns $ M.insert playerId PlayerConnection {connection = c, pendingEvents = chan} oldConns
        _ <- forkIO $ createHandler playerId conns conn addr
        yield
        connectionWaitLoop (playerId+1)
  forkIO $! connectionWaitLoop 0
