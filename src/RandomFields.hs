module RandomFields (ununiqueMatIndex, uniqueMatIndex, RandomSeed, randomList) where

import System.Random ( mkStdGen, Random(randoms) )

type RandomSeed = Int

integerToNat :: Integral a => a -> a
integerToNat x = if x >= 0 then 2*x else (-2) * x - 1

integerFromNat :: Integral a => a -> a
integerFromNat x = if even x then x`div`2 else (x + 1)`div`(-2)

uniqueMatIndex :: Integral a => a -> a -> a
uniqueMatIndex x' y' = let x = integerToNat x'
                           y = integerToNat y'
                           n = (x+y) in ((1+n)*n) `div` 2 + x

ununiqueMatIndex :: Integral a => a -> (a, a)
ununiqueMatIndex x = let g = floor $ (sqrt (8 * (fromIntegral x :: Double) + 1) - 1) / 2
                         fp nn = ((1+nn)*nn) `div` 2
                         n = x - fp g in
                       (integerFromNat n, integerFromNat $ g - n)

randomList :: RandomSeed -> [Int]
randomList = randoms . mkStdGen
