{-# LANGUAGE ScopedTypeVariables #-}
-- | SDL wrapper module
module Sdl(Sdl(getWindow, getTextures),
           SdlWindow, SdlRenderer,
           SdlEvent(SdlEventUnknown, SdlQuitEvent, SdlKeyboardEvent),
           time, win, state, keyRepeat, sym, keycode,
           sdlGetWindowSize,
           red, green, gray,
           initSdlInternal,
           renderSdl,
           sdlFillRect,
           sdlRenderTexture,
           sdlClear,
           forEvents,
           TextureType(..), Textures, Tex(Tex)
          ) where

import Foreign.C.String
import Foreign ( alloca, Int32 )
import Foreign.Ptr ( Ptr, nullPtr, castPtr, plusPtr )
import Foreign.Storable ( Storable, sizeOf, alignment, peek, poke )
import Data.Word
import qualified Data.Map as M
import qualified Data.ByteString.Lazy as B
import Control.Exception (catch, SomeException)

import GameUtils
import qualified Data.ByteString.Unsafe as B(unsafeUseAsCString)

data TextureType = TextureField FieldType
                 | TexturePlayer Int Direction
                 deriving (Show, Eq, Ord)
data Tex = Tex Int Int Int String
type Textures = M.Map TextureType [Tex]

foreign import ccall unsafe "SDL_Init" sdlInit :: Int -> IO Int
foreign import ccall unsafe "SDL_GetError" sdlGetError' :: IO CString
foreign import ccall unsafe "SDL_CreateWindow" sdlCreateWindow'
        :: CString -> Int -> Int -> Int -> Int -> Word32 -> IO SdlWindow
foreign import ccall unsafe "SDL_CreateRenderer" sdlCreateRenderer
        :: SdlWindow -> Int -> Word32 -> IO SdlRenderer
foreign import ccall unsafe "SDL_PollEvent" sdlPollEvent :: Ptr SdlEvent -> IO Int
foreign import ccall unsafe "SDL_RenderPresent" sdlRenderPresent :: SdlRenderer -> IO ()
foreign import ccall unsafe "SDL_RenderFillRect" sdlRenderFillRect :: SdlRenderer ->
  Ptr SdlRect -> IO ()
foreign import ccall unsafe "SDL_RenderCopy" sdlRenderCopy :: SdlRenderer -> SdlTexture ->
  Ptr SdlRect -> Ptr SdlRect -> IO ()
foreign import ccall unsafe "SDL_SetRenderDrawColor" sdlSetRenderDrawColor :: SdlRenderer ->
  Word8 -> Word8 -> Word8 -> Word8 -> IO ()
foreign import ccall unsafe "SDL_RenderClear" sdlRenderClear :: SdlRenderer -> IO ()
-- foreign import ccall unsafe "SDL_Delay" sdlDelay :: Word32 -> IO ()
foreign import ccall unsafe "SDL_GetWindowSize" sdlGetWindowSize' :: SdlWindow -> Ptr Int32 -> Ptr Int32 -> IO ()
foreign import ccall unsafe "SDL_CreateRGBSurfaceFrom" sdlCreateRGBSurfaceFrom :: Ptr () -> Int32 -> Int32 -> Int32 -> Int32 -> Word32 -> Word32 -> Word32 -> Word32 -> IO SdlSurface
foreign import ccall unsafe "SDL_CreateTextureFromSurface" sdlCreateTextureFromSurface :: SdlRenderer -> SdlSurface -> IO SdlTexture
foreign import ccall unsafe "SDL_FreeSurface" sdlFreeSurface :: SdlSurface -> IO ()

data SdlRect = SdlRect { getX :: Int, getY :: Int, getWidth :: Int, getHeight :: Int } deriving Show
data Color = Color {getRed :: Word8, getGreen :: Word8, getBlue :: Word8, getAlpha ::Word8}

-- | red 'Color' (#ff000000)
red :: Color
red = Color {getRed = 255, getGreen = 0, getBlue = 0, getAlpha = 255}

-- | green 'Color' (#00ff0000)
green :: Color
green = Color {getRed = 0, getGreen = 255, getBlue = 0, getAlpha = 255}

gray :: Color
gray = Color {getRed = 128, getGreen = 128, getBlue = 128, getAlpha = 255}

-- | Get the current size of the 'SdlWindow'
sdlGetWindowSize :: SdlWindow -- ^ the 'SdlWindow'
  -> IO (Int32, Int32) -- ^ an 'IO' action containing (@width@, @height@)
sdlGetWindowSize window = alloca $ \xptr ->
  alloca $ \yptr -> do sdlGetWindowSize' window xptr yptr
                       x <- peek xptr
                       y <- peek yptr
                       return (x, y)

sdlWindowResizable :: Word32
sdlWindowResizable = 0x20

instance Storable SdlRect where
  sizeOf = const 32
  alignment = const 32
  peek _ptr = undefined
  poke ptr val = do
    poke (castPtr ptr :: Ptr Int) $ getX val
    poke (castPtr $ plusPtr ptr 4 :: Ptr Int) $ getY val
    poke (castPtr $ plusPtr ptr 8 :: Ptr Int) $ getWidth val
    poke (castPtr $ plusPtr ptr 12 :: Ptr Int) $ getHeight val

-- | An SDL Event from the user
data SdlEvent =
  SdlQuitEvent
  { -- ^ User window quit request
    time :: Word32 -- ^ the time at which the event was requested
  }
  | SdlKeyboardEvent -- ^ a key input event
    {
      time :: Word32, -- ^ the time at which the event was requested
      win :: Word32, -- ^ the window currently focused
      state :: Bool, -- ^ on/off
      keyRepeat :: Bool, -- ^ repeated?
      sym :: SdlKeysym -- ^ the key symbol
    }
  | SdlEventUnknown { typ :: Word32 }
  deriving Show

data SdlKeysym = SdlKeysym
  {
    scancode :: Word32,
    keycode :: Int32,
    keymod :: Word16
  } deriving Show

-- | Make 'SdlEvent' 'Storable' to use in in conjunction with 'Ptr's
instance Storable SdlEvent where
  sizeOf = const 56
  alignment = const 64
  peek ptr = do
    v <- peek (castPtr ptr :: Ptr Word32)
    case v of
      0x100 -> SdlQuitEvent <$> peek (castPtr (plusPtr ptr 4) :: Ptr Word32)
      0x300 -> do
        tm <- peek (castPtr (plusPtr ptr 4) :: Ptr Word32)
        wn <- peek (castPtr (plusPtr ptr 8) :: Ptr Word32)
        stat <- (/=0)<$> peek (castPtr (plusPtr ptr 12) :: Ptr Word8)
        rep <- (/=0)<$> peek (castPtr (plusPtr ptr 13) :: Ptr Word8)
        sym' <- peek (castPtr (plusPtr ptr 16) :: Ptr SdlKeysym)
        return $ SdlKeyboardEvent { time = tm, win = wn, state = stat, keyRepeat = rep, sym = sym' }
      a -> return $ SdlEventUnknown a
  poke ptr (SdlEventUnknown i) = poke (castPtr ptr :: Ptr Word32) i
  poke _ _ = error "poke SdlEvent"


-- | Make 'SdlKeysym' 'Storable' to use in in conjunction with 'Ptr's
instance Storable SdlKeysym where
  sizeOf = const 16
  alignment = const 16
  peek ptr = do
    sc <- peek (castPtr ptr :: Ptr Word32)
    kc <- peek (castPtr (plusPtr ptr 4) :: Ptr Int32)
    ms <- peek (castPtr (plusPtr ptr 8) :: Ptr Word16)
    return $ SdlKeysym {
      scancode = sc,
      keycode = kc,
      keymod = ms }
  poke _ _ = error "poke SdlKeysym"


sdlGetError :: IO String
sdlGetError = sdlGetError' >>= peekCString

type X      = Int
type Y      = Int
type Width  = Int
type Height = Int

type SdlWindow = Ptr ()
type SdlRenderer = Ptr ()
type SdlTexture = Ptr ()
type SdlSurface = Ptr ()

sdlCreateWindow :: String -> X -> Y -> Width -> Height -> Word32 -> IO SdlWindow
sdlCreateWindow str x y w h f = withCString str
  (\cstr -> sdlCreateWindow' cstr x y w h f)

-- | Represents a single SDL instance
data Sdl = Sdl {getWindow :: SdlWindow, -- ^ The window
                getRenderer :: SdlRenderer, -- ^ The main renderer
                getTextures :: M.Map TextureType [(Int, SdlTexture)]
               }
  deriving Show

sdlInitVideo :: Int
sdlInitVideo = 32

newtype EitherT m a b = EitherT (m (Either a b))
type EitherIO = EitherT IO

instance Functor m => Functor (EitherT m a) where
  fmap f (EitherT e) = EitherT ((f<$>) <$> e)

instance Monad m => Applicative (EitherT m a) where
  pure = EitherT . pure . pure
  (EitherT f') <*> (EitherT a') = EitherT
    $ f' >>= \f -> a' >>= \a ->
    return $ f <*> a

instance Monad m => Monad (EitherT m a) where
  (EitherT m') >>= f = EitherT $ do
    m <- m'
    let (EitherT x) = case m of
          Left a -> EitherT . return . Left $ a
          Right b -> f b
    x

instance Monad m => MonadFail (EitherT m String) where
  fail = EitherT . return . Left

failOn :: Monad m => (a -> Bool) -> (a -> m b) -> m a -> EitherT m b a
failOn p l a = EitherT $ do
  v <- a
  if p v then
    return $ Right v
  else
    Left <$> l v

runEitherT :: EitherT m a b -> m (Either a b)
runEitherT (EitherT x) = x

catchEither :: IO a -> EitherIO String a
catchEither act = EitherT $ catch (Right <$> act) (\(e :: SomeException) -> fail $ show e)

loadTexture :: SdlRenderer -> Tex -> EitherIO String (Int, SdlTexture)
loadTexture renderer (Tex w' h' i file) = do
  let w = fromIntegral w'
      h = fromIntegral h'
  contents <- catchEither $ B.readFile file
  texture <- failOn (/= nullPtr) (const sdlGetError) $ B.unsafeUseAsCString (B.toStrict contents) $ \ptr -> do
             surface <- sdlCreateRGBSurfaceFrom (castPtr ptr) w h 32 (w * 4) 0x000000ff 0x0000ff00 0x00ff0000 0xff000000
             texture <- sdlCreateTextureFromSurface renderer surface
             sdlFreeSurface surface
             return texture
  return (i, texture)

-- | Initializes 'Sdl' state, creating default 'SdlWindow' and 'SdlRenderer'
--
-- This action will return an 'IO' ('Left' 'String') containing an error message
-- if 'Sdl' initialization failed unexpectedly.
initSdlInternal :: Textures -> IO (Either String Sdl)
initSdlInternal textures = runEitherT $ do
  _ <- failOn (== 0) (const sdlGetError) $ sdlInit sdlInitVideo
  window <- failOn (/=nullPtr) (const sdlGetError)
    $ sdlCreateWindow "Haskell RPG... don't know any nane" 0 0 100 100 sdlWindowResizable
  renderer <- failOn (/=nullPtr) (const sdlGetError)
    $ sdlCreateRenderer window (-1) 0

  loadedTextures <- mapM (mapM (loadTexture renderer)) textures --M.fromList . zip images <$> mapM loadTexture images

  return Sdl { getWindow = window, getRenderer = renderer, getTextures = loadedTextures }

-- | Renders the current 'Sdl' state to the screen using __@void sdlRenderPresent(void);@__
renderSdl :: Sdl -> IO ()
renderSdl = sdlRenderPresent . getRenderer

-- | Fills a rectange in the 'Sdl' context with given coordinates and color
sdlFillRect ::
  Int      -- ^ __x1__: the x position of the top-left corner
  -> Int   -- ^ __y1__: the y position of the top-left corner
  -> Int   -- ^ __w__: the width of the rectangle
  -> Int   -- ^ __h__: the height of the rectangle
  -> Color -- ^ __color__: the 'Color' of the rectangle
  -> Sdl   -- ^ __ctx__: the 'Sdl' context
  -> IO () -- ^ __returns__: an IO action
sdlFillRect x y w h color sdl = alloca $ \p -> do
  sdlSetRenderDrawColor (getRenderer sdl) (getRed color) (getGreen color)
    (getBlue color) (getAlpha color)
  poke p $ SdlRect x y w h
  sdlRenderFillRect (getRenderer sdl) p


-- | Renders a texture in the 'Sdl' context with given coordinates
sdlRenderTexture ::
  Int      -- ^ __x1__: the x position of the top-left corner
  -> Int   -- ^ __y1__: the y position of the top-left corner
  -> Int   -- ^ __w__: the width of the texture
  -> Int   -- ^ __h__: the height of the texture
  -> SdlTexture -- ^ __texture__: the 'SdlTexture' to render
  -> Sdl   -- ^ __ctx__: the 'Sdl' context
  -> IO () -- ^ __returns__: an IO action
sdlRenderTexture x y w h texture sdl = alloca $ \p -> do
  poke p $ SdlRect x y w h
  sdlRenderCopy (getRenderer sdl) texture nullPtr p

-- | Clears the 'Sdl' window
sdlClear :: Sdl -> IO ()
sdlClear sdl = do
  sdlSetRenderDrawColor (getRenderer sdl) 0 0 0 255
  sdlRenderClear $ getRenderer sdl

-- | calls __f__ with all events, and exits on a 'False' value
--
-- ==== __Examples__
-- Check for 'SdlQuitEvent', ignore everything else
-- 
-- >>> forEvents (undefined :: s) (\s e -> case e of (SdlQuitEvent _) -> return (True, s, []); _ -> return (False, s, []))
-- IO (True, {State ...}, [])
forEvents :: s -> (s -> SdlEvent -> IO (Bool, s, [GameEvent])) -- ^ __f__: the function, if
  -- returning 'IO' 'True', short-curcuit execution
  -> IO (Bool, s, [GameEvent]) -- ^ __returns__: 'True' if any function short-curcuited execution, 'False' otherwise
forEvents st f = alloca $ \x -> do
     poke x (SdlEventUnknown 0)
     v <- sdlPollEvent x
     if v /= 0 then do
       (a, s, ev) <- peek x >>= f st
       if not a then do
         (a', s', ev') <- forEvents s f
         return (a', s', ev ++ ev')
       else
        return (True, s, ev)
     else
        return (False, st, [])
