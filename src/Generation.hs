module Generation(RandomSeed, generateFieldAt, uniqueMatIndex) where

import GameUtils (FieldType (Stone, Grass))
import RandomFields (RandomSeed, randomList, ununiqueMatIndex, uniqueMatIndex)
import System.Random (random, mkStdGen)

scale :: Double
scale = 12

internalRandomList :: RandomSeed -> [(Int, Double)]
internalRandomList seed = zipWith (\x a -> (a, perlin seed scale x)) [0..] $ randomList seed
-- randomFields :: [FieldType] -> RandomSeed -> Int -> [FieldType]
-- randomFields = wfc

generateFieldAt :: RandomSeed -> Int -> FieldType
generateFieldAt s idx = if snd (internalRandomList s !! idx) < 0 then Grass else Stone

-- nbidx :: (Integral a1, Integral a2) => a1 -> a2 -> [(a1, a2)]
-- nbidx a b = [(a-1,b-1), (a-1,b), (a-1,b+1), (a,b-1), (a,b+1), (a+1,b-1), (a+1,b), (a+1,b+1)]


lerp :: Double -> Double -> Double -> Double
lerp a b x = a + x * (b - a)

fade :: Double -> Double
fade f = 6 * f**5 - 15 * f**4 + 10 * f**3

(%) :: Integral a => a -> a -> a
(%) = mod

gradient :: Int -> Double -> Double -> Double
gradient c x y = case c % 8 of
            0 ->  x + y
            1 ->  x
            2 ->  x - y
            3 -> (-y)
            4 -> (-x) - y
            5 -> (-x)
            6 -> (-x) + y
            7 ->  y
            _ -> undefined

seedOffset :: Int
seedOffset = 1000

perlin :: RandomSeed -> Double -> Int -> Double
perlin seed' scale' idx = perlin''
  where
    (x, y) = ununiqueMatIndex idx
    seed = (fst $ random $ mkStdGen seed') % 2147483648
    dx = fromIntegral x / scale' + fromIntegral (seed * seedOffset) + 0.1
    dy = fromIntegral y / scale' + fromIntegral (seed * seedOffset) + 0.1
    perlin'' = perlin' dx dy

(!/) :: [Int] -> Int -> Int
(!/) = flip $ flip (!!) . (% 255)

perlin' :: Double -> Double -> Double
perlin' x y = lerp x1 x2 yf
  where xi = floor x
        yi = floor y
        xg = x - fromIntegral xi
        yg = y - fromIntegral yi
        xf = fade xg
        yf = fade yg
        n00 = gradient (fromIntegral $ perms !/ ((perms !/ xi) + yi)) xg yg
        n01 = gradient (fromIntegral $ perms !/ ((perms !/ xi) + yi + 1)) xg (yg-1)
        n10 = gradient (fromIntegral $ perms !/ ((perms !/ (xi+1)) + yi)) (xg-1) yg
        n11 = gradient (fromIntegral $ perms !/ ((perms !/ (xi+1)) + yi + 1)) (xg-1) (yg-1)
        x1 = lerp n00 n10 xf
        x2 = lerp n01 n11 xf

perms :: [Int]
perms = [ 151, 160, 137, 91, 90, 15, 131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 
                      103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23, 190, 6, 148, 247, 120, 234, 75, 0, 
                      26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33, 88, 237, 149, 56, 
                      87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166, 
                      77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 
                      46, 245, 40, 244, 102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 
                      187, 208, 89, 18, 169, 200, 196, 135, 130, 116, 188, 159, 86, 164, 100, 109, 
                      198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123, 5, 202, 38, 147, 118, 126, 
                      255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42, 223, 183, 
                      170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 
                      172, 9, 129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 
                      104, 218, 246, 97, 228, 251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 
                      241, 81, 51, 145, 235, 249, 14, 239, 107, 49, 192, 214, 31, 181, 199, 106, 
                      157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254, 138, 236, 205, 
                      93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180]


-- type WFCField a = (Int, [a])

-- data Operation = Equal
-- data Rule value = Neighbours Operation value Int
--                 | Always

-- ruleMatches :: (WFCElement a, Eq a) => Integral i => M.Map i a -> i -> i -> Rule a -> Bool
-- ruleMatches xs x y (Neighbours Equal val i) = isValidMatch
--   where
--     nbrs = nbidx x y
--     vals = mapMaybe ((xs M.!?). uncurry uniqueMatIndex) nbrs
--     matchingVals = length $ filter (==val) vals
--     possibleExtraMatching = length nbrs - length vals
--     neededExtraMatching = i - matchingVals
--     isValidMatch = neededExtraMatching >= 0 && neededExtraMatching <= possibleExtraMatching

-- ruleMatches _ _ _ Always = True

-- class Show a => WFCElement a where
--   initialWFCElements :: [a]
--   isValid :: M.Map Int a -> Int -> a -> Bool
--   getRule :: a -> Rule a

-- {-
-- case x of
-- Grass -> True
-- Stone -> length (filter (==Stone) $ mapMaybe ((xs M.!?). uncurry uniqueMatIndex) $ nbidx a b) == 2
-- -}

-- instance WFCElement FieldType where
--   initialWFCElements = [Grass, Stone]
--   isValid xs i x = let (a,b) = ununiqueMatIndex i
--                    in ruleMatches xs a b $ getRule x
--   getRule Stone = Neighbours Equal Stone 2
--   getRule _ = Always

-- initialWFCList :: WFCElement a => RandomSeed -> [WFCField a]
-- initialWFCList = map (,initialWFCElements) . internalRandomList

-- collapseRandom :: WFCElement a => WFCField a -> Maybe a
-- collapseRandom (_, []) = Nothing
-- collapseRandom (seed, xs) = Just $ xs !! (seed `mod` length xs)

-- filterField :: WFCElement a => M.Map Int a -> Int -> WFCField a -> WFCField a
-- filterField env i (s, xs) = (s, filter (isValid env i) xs)

-- allAffectedValid :: WFCElement a => M.Map Int a -> Int -> Bool
-- allAffectedValid xs idx = all (uncurry (isValid xs)) vals
--   where
--     nbrs = uncurry nbidx $ ununiqueMatIndex idx
--     vals = mapMaybe (sequence . (\x -> (x, xs M.!? x)) . uncurry uniqueMatIndex) nbrs

-- inverse :: (Eq a, WFCElement a) => [a] -> [a]
-- inverse xs = filter (`notElem` xs) initialWFCElements

-- collapseAffected :: (Eq a, WFCElement a) => RandomSeed -> WFC a -> Int -> WFC a
-- collapseAffected seed (xs, ps) idx = collapsedMap
--   where
--     nbrs = uncurry nbidx $ ununiqueMatIndex idx
--     vals = map fst . filter (isNothing . snd) $ map ((\x -> (x, xs M.!? x)) . uncurry uniqueMatIndex) nbrs
--     collapsedMap = foldl (flip collapseIfOne) (xs, ps) vals
--     collapseIfOne i (ys, qs) = let (_, arr) = initialWFCList seed !! i in
--       if length arr /= 1 then
--         (ys, M.insert i (inverse arr) qs)
--       else (M.insert i (head arr) ys, qs)


-- -- nbrn :: (Integral a1, Integral a2) => Int -> a1 -> a2 -> [(a1, a2)]
-- -- nbrn 0 _ _ = []
-- -- nbrn 1 a b = nbidx a b
-- -- nbrn n a b = nbrn (n-1) a b >>= uncurry nbidx

-- wfc :: (Eq a, WFCElement a) => WFC a -> RandomSeed -> Int -> WFC a
-- wfc cs seed idx = wfcRegion seed idx 1 cs
--   -- where nbrs = map (uncurry uniqueMatIndex) $ uncurry (nbrn 0) $ ununiqueMatIndex idx
--   --       generateAll = f (idx:nbrs)
--   --       f [] s = s
--   --       f (x:xs) s = let ns =  in f xs ns

-- wfcRegion :: (Eq a, WFCElement a) => RandomSeed -> Int -> Int -> WFC a -> WFC a
-- wfcRegion s idx sz current = endVal'
--   where (x, y) = ununiqueMatIndex idx
--         ids = map (uncurry uniqueMatIndex) $ [(a, b) | a <- [(x-sz)..(x+sz)], b <- [(y-sz)..(y+sz)]]
--         endVal = foldM (flip $ wfc' s) current ids
--         endVal' = case endVal of
--           Left (vs, _ivs) -> let ls = filter (\(_, b) -> b `notElem` fst current) $ M.toList vs
--                                  v = if null ls then current else
--                                        let (k, pivot) = head ls in
--                                          second (update k (maybe [pivot] (pivot:))) current
--                                    in wfcRegion (s*2) idx sz v
--           Right r -> r

-- type WFC a = (M.Map Int a, M.Map Int [a])

-- update :: Ord k => k -> (Maybe v -> v) -> M.Map k v -> M.Map k v
-- update k f m = M.insert k (f v) m
--   where v = m M.!? k

-- -- allValid :: WFCElement v => M.Map Int v -> Bool
-- -- allValid m = all (uncurry (isValid m)) $ M.toList m

-- maybeToEither :: a -> Maybe b -> Either a b
-- maybeToEither d = maybe (Left d) Right

-- wfc' :: (Eq a, WFCElement a) => RandomSeed -> Int -> WFC a -> Either (WFC a) (WFC a)
-- wfc' seed idx = f
--   where f cwfc@(cs, ps) = case cs M.!? idx of
--             Just _ -> return (cs, ps)
--             Nothing -> do
--               let inval = fromMaybe [] (ps M.!? idx)
--                   elems = initialWFCList seed !! idx
--                   elemsFiltered  = second (filter (not . (`elem` inval))) elems
--                   elemsFiltered' = filterField cs idx elemsFiltered
--               collapsed <- maybeToEither cwfc $ collapseRandom elemsFiltered'
--               let newMap = M.insert idx collapsed cs
--               if allAffectedValid newMap idx then return (collapseAffected seed (newMap, ps) idx)
--                 else f $ second (update idx (maybe [collapsed] (collapsed:))) cwfc
