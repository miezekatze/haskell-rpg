-- {-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -Wno-deferred-out-of-scope-variables #-}
-- | The Main module
--
-- represents the entirety of the game
module Main where

-- import Praeludium

import Control.Monad ( unless, when )
import Network.Socket (tupleToHostAddress)

import Control.Concurrent.STM.TChan (newTChan)
import Control.Concurrent.STM (atomically)
import Data.IORef (newIORef, readIORef, writeIORef)
import Control.Concurrent (threadDelay)

import qualified Data.Map as M
import GameServer ( startServer )
import GameClient
    ( emmitEvent, updatePendingState, defaultState, runClientThread, State (you), getEntityAt, ded )
import Render ( handleEvents, initSdl, renderState, Textures, TextureType(..), Tex(Tex) )
import GameUtils ( FieldType(Grass, Stone), Direction(..) )
import Data.Maybe (isJust)

defaultTexture :: Int -> [Char] -> Tex
defaultTexture i = Tex 16 16 i . ("assets/"++)

playerTexture :: Int -> [Char] -> Tex
playerTexture i = Tex 16 32 i . ("assets/"++)

imageFiles :: Textures
imageFiles = M.fromList [(TextureField Grass, map (uncurry defaultTexture) [(70, "grass3"),
                                                                            (10, "grass0"),
                                                                            (5, "grass1"),
                                                                            (15, "grass2")]),
                         (TextureField Stone, map (uncurry defaultTexture) [(1, "stone0")]),
                         (TexturePlayer 0 DDown, map (uncurry playerTexture) [(0, "playerd0")]),
                         (TexturePlayer 0 DLeft, map (uncurry playerTexture) [(0, "playerl0")]),
                         (TexturePlayer 0 DUp, map (uncurry playerTexture) [(0, "playeru0")]),
                         (TexturePlayer 0 DRight, map (uncurry playerTexture) [(0, "playerr0")])
                        ]

-- | The main IO action
--
-- This run the game from begin to end
main :: IO ()
main = do
  sdl <- initSdl imageFiles
  gameServer <- startServer
  let _ = gameServer

  chan <- atomically newTChan
  
  sock <- runClientThread
    (tupleToHostAddress (127, 0, 0, 1))
    chan

  iostate <- defaultState sdl >>= newIORef
  let f = do
        state <- readIORef iostate
        (x, state', ev) <- handleEvents state
        mapM_ (($ sock) . emmitEvent) ev

        when (isJust $ getEntityAt (you state) state) $ renderState state'
        writeIORef iostate state'
        
        updatePendingState iostate chan

        isDed <- ded <$> readIORef iostate

        threadDelay $ (1000*1000) `div` 60
        unless (x || isDed) f
  f
  putStrLn "exiting game.."
