{-# LANGUAGE ScopedTypeVariables, LambdaCase #-}

-- | Utility functions used by server and client
module GameUtils(GameEvent(..),
                 Time, FieldType(Grass, Stone),
                 Player(..),
                 IsEntity(..),
                 Pos, Direction(..),
                 direction2Int,
                 handleGameEvent,
                 getTime,
                 fieldType2Int,
                 eventConnect, eventYourPosition, eventExploreField, eventMoveRequest,
                 eventAttack, eventHealth, eventDeath, eventJoin,
                 EntityType(..),
                 EntityImpl(..),
                 (...), (.-),
                 getPosition, setPosition, getEntityId, getDirection,
                 getHealth, getMaxHealth, setHealth,
                 convertEntityImpl,
                 downcast,
                 Entity(..)) where

import Data.Binary (decode, Binary, Word8)
import Network.Socket (Socket)
import Data.Int (Int32)

import qualified Network.Socket.ByteString as NB
import qualified Data.ByteString as BS
import Data.ByteString.Lazy (fromStrict)
import Control.Concurrent.STM (TChan, atomically, writeTChan)
import Data.Time.Clock.POSIX (getPOSIXTime)
import Data.Data (TypeRep, typeOf)
import Data.Typeable (Typeable)
import Unsafe.Coerce (unsafeCoerce)

-- | Data type used for time / delta time values
type Time = Int32

-- | The type of a game field
data FieldType = Grass -- ^ a simple grass field
               | Stone -- ^ a simple stone field (currenlty just for testing)
               deriving (Show, Eq, Ord)        -- ^ just for debugging

-- | coverts a 'FieldType' to an 'Integral' a, useful for network serialization
fieldType2Int :: Integral a => FieldType -> a
fieldType2Int = \case Grass -> 0
                      Stone -> 1

int2FieldType :: (Integral a, Show a) => a -> FieldType
int2FieldType = \case 0 -> Grass
                      1 -> Stone
                      x -> error $ "invalid field type: " ++ show x

-- | A game event sent over the network (client \<-\> server)
data GameEvent = Connect        -- ^ __client -> server__: initialize connection to server, server should
                                -- send position, spawn fields, etc
               | YourPosition   -- ^ __server -> client__: set absolute player position to (x, y)
                 Int32          -- ^ __@x@__: horizontal position on the map
                 Int32          -- ^ __@y@__: vertical position on the map
                 Int32          -- ^ __@dt@__: delta time since last tick
                 Direction      -- ^ __@dir@__: direction of last player move
               | Join           -- ^ __server -> client__: client joined the game
                 Int32          -- ^ __@pid@__: the pid of client
                 Int32          -- ^ __@x@__: x position of the client player
                 Int32          -- ^ __@y@__: y position of the client player
               | ExploreField   -- ^ __server -> client__: information about discovered field
                 (Int32, Int32) -- ^ __@(x, y)@__: position of the discovered field
                 FieldType      -- ^ __@fieldType@__: the type of the discovered field (e. g. @'Grass'@)
               | MoveRequest    -- ^ __client -> server__: request move in given direction
                 Int32          -- ^ __@dx@__: relative x movement
                 Int32          -- ^ __@dy@__: relative y movement
               | Attack         -- ^ __client -> server__: attack a given entity with current holding weapon
                 Int32          -- ^ __@id@__: entity id
               | Health         -- ^ __server -> client__: set health of entity
                 Int32          -- ^__@id@__: entity id
                 Int32          -- ^__@health@__: the next health
               | Death         -- ^ __server -> client__: an entity died
                 Int32          -- ^__@id@__: entity id
               | Log            -- ^ __no transmit__: logging message to console instead of sending message
                 String         -- ^ __@msg@__: the message to log
        deriving Show                 -- ^ for debugging purposes (e. g. 'print')

-- | constant representing numerical value of 'Connect' 'GameEvent'
eventConnect :: Int32
eventConnect = 0

-- | constant representing numerical value of 'YourPosition' 'GameEvent'
eventYourPosition :: Int32
eventYourPosition = 1

-- | constant representing numerical value of 'ExploreField' 'GameEvent'
eventExploreField :: Int32
eventExploreField = 2

-- | constant representing numerical value of 'MoveRequest' 'GameEvent'
eventMoveRequest :: Int32
eventMoveRequest = 3

-- | constant representing numerical value of 'Attack' 'GameEvent'
eventAttack :: Int32
eventAttack = 4

-- | constant representing numerical value of 'Health' 'GameEvent'
eventHealth :: Int32
eventHealth = 5

-- | constant representing numerical value of 'Death' 'GameEvent'
eventDeath :: Int32
eventDeath = 6

-- | constant representing numerical value of 'Join' 'GameEvent'
eventJoin :: Int32
eventJoin = 7

class Sized a where
  size :: a -> Int

instance Sized Int32 where
  size = const 4

instance Sized Word8 where
  size = const 1

instance Sized Double where
  size = const 25

readBytes :: forall a. (Binary a, Sized a) => Socket -> IO (Maybe a)
readBytes sock = do
  bts <- NB.recv sock (fromIntegral bytes)
  let len = BS.length bts
  f len bts
  where
    bytes = size (undefined :: a)
    f len bts
      | len /= bytes = return Nothing
      | otherwise = let x = decode (fromStrict bts) in return $ Just x


convertGameEvent :: Int32 -> TChan GameEvent -> Socket -> IO ()
convertGameEvent typ chan sock = do
  case typ of
    ty | ty == eventConnect -> do
           atomically $ writeTChan chan Connect
       | ty == eventYourPosition -> do
          x <- readBytes sock :: IO (Maybe Int32)
          y <- readBytes sock :: IO (Maybe Int32)
          dt <- readBytes sock :: IO (Maybe Int32)
          dir' <- readBytes sock :: IO (Maybe Word8)
          let vals = x >>= \a -> y >>= \b -> dt >>= \c -> dir' >>= \dir -> return (a, b, c, dir)
          case vals of
            Just (a, b, c, dir) -> atomically $ writeTChan chan $ YourPosition a b c $ directionFromInt dir
            _ -> error "invalid read" >> return ()
       | ty == eventExploreField -> do
          x <- readBytes sock :: IO (Maybe Int32)
          y <- readBytes sock :: IO (Maybe Int32)
          f <- readBytes sock :: IO (Maybe Int32)
          let vals = x >>= \a -> y >>= \b -> f >>= \ft -> return (a, b, ft)
          case vals of
            Just (a, b, ft) -> atomically $ writeTChan chan $ ExploreField (a, b) $ int2FieldType ft
            _ -> error "invalid read" >> return ()
       | ty == eventMoveRequest -> do
          x <- readBytes sock :: IO (Maybe Int32)
          y <- readBytes sock :: IO (Maybe Int32)
          let vals = x >>= \a -> y >>= \b -> return (a, b)
          case vals of
            Just (a, b) -> atomically $ writeTChan chan $ MoveRequest a b
            _ -> error "invalid read" >> return ()
       | ty == eventAttack -> do
          pid <- readBytes sock :: IO (Maybe Int32)
          case pid of
            Just a -> atomically $ writeTChan chan $ Attack a
            _ -> error "invalid read" >> return ()
       | ty == eventHealth -> do
          eid <- readBytes sock :: IO (Maybe Int32)
          hlt <- readBytes sock :: IO (Maybe Int32)
          let vals = eid >>= \a -> hlt >>= \b -> return (a, b)
          case vals of
            Just (a, b) -> atomically $ writeTChan chan $ Health a b
            _ -> error "invalid read" >> return ()
       | ty == eventDeath -> do
          eid <- readBytes sock :: IO (Maybe Int32)
          case eid of
            Just a -> atomically $ writeTChan chan $ Death a
            _ -> error "invalid read" >> return ()
       | ty == eventJoin -> do
          pid <- readBytes sock :: IO (Maybe Int32)
          x <- readBytes sock :: IO (Maybe Int32)
          y <- readBytes sock :: IO (Maybe Int32)
          let vals = sequence [pid, x, y]
          case vals of
            Just [a, b, c] -> atomically $ writeTChan chan $ Join a b c
            _ -> error "invalid read" >> return ()
       | otherwise ->
         putStrLn $ "[warn] Event " ++ show typ ++ " not handled yet."


-- | Wait for events on socket, convert them to 'GameEvent's and send them through chan
--
-- This 'IO' action never halts. Users are highly encouraged to run it using 'Control.Concurrency.forkIO', like so:
-- 
-- @
-- 'Control.Concurrency.forkIO' $ handleGameEvent sock chan
-- @
handleGameEvent :: Socket -- ^ the 'Socket' used to listen for events
  -> TChan GameEvent      -- ^ the 'TChan' to send 'GameEvent's to
  -> IO ()                -- ^ returns 'IO' action
handleGameEvent sock chan = do
  typ <- readBytes sock
  case typ of
    Just t -> convertGameEvent t chan sock >> handleGameEvent sock chan
    Nothing -> error "invalid read" >> return ()

-- | Get the current UNIX time in milliseconds
getTime :: IO Time
getTime = fmap (round . (*1000)) getPOSIXTime

type Pos = (Int, Int)
data Direction = DLeft | DRight | DUp | DDown
  deriving (Show, Eq, Ord)

directionFromInt :: Integral a => a -> Direction
directionFromInt 0 = DLeft
directionFromInt 1 = DRight
directionFromInt 2 = DUp
directionFromInt 3 = DDown
directionFromInt _ = undefined

direction2Int :: Direction -> Word8
direction2Int DLeft = 0
direction2Int DRight = 1
direction2Int DUp = 2
direction2Int DDown = 3

class (Show a, Typeable a) => IsEntity a where
  getEntityImpl :: a -> EntityImpl a

  getEntityType :: a -> TypeRep
  getEntityType a = typeOf a

  getInner :: a -> Entity
  getInner = Entity

data EntityImpl a = EntityImpl {
  getPosition' :: Pos,
  setPosition' :: Pos -> Direction -> a,
  getEntityId' :: Int,
  getDirection' :: Direction,
  getHealth' :: Int,
  getMaxHealth' :: Int,
  setHealth' :: Int -> a
  }

data Player = Player {
  getPlayerId :: Int,
  playerPosition :: Pos,
  getPlayerDirection :: Direction,
  getPlayerHealth :: Int,
  getPlayerName :: String
  } deriving Show

instance IsEntity Player where
  getEntityImpl p = EntityImpl {
    getPosition'  = playerPosition p,
    setPosition'  = \a dir -> p { playerPosition = a, getPlayerDirection = dir },
    getEntityId'  = getPlayerId p,
    getDirection' = getPlayerDirection p,
    getMaxHealth' = 100,
    getHealth'    = getPlayerHealth p,
    setHealth'    = \a -> p {getPlayerHealth = a}
    }

newtype EntityType = EntityPlayer Player deriving Show

-- onEntity :: (forall e. IsEntity e => e -> t) -> EntityType -> t
-- onEntity g a = case a of
--   EntityPlayer p -> g p

-- onEntityM :: (forall e. IsEntity e => e -> e) -> EntityType -> EntityType
-- onEntityM g a = case a of
--   EntityPlayer p -> EntityPlayer (g p)
  
(...) :: ((forall t1. const t1 => t1 -> t1) -> t -> t) -> (forall t1. const t1 => a -> b -> t1 -> t1) -> a -> b -> t -> t
(...) f g a b = f (g a b)

(.-) :: ((forall t1. const t1 => t1 -> t1) -> t -> t) -> (forall t1. const t1 => a -> t1 -> t1) -> a -> t -> t
(.-) f g a = f (g a)

-- instance IsEntity EntityType where
--   getPosition = onEntity getPosition
--   setPosition = onEntityM ... setPosition
--   getEntityId = onEntity getEntityId
--   getDirection = onEntity getDirection
--   getHealth = onEntity getHealth
--   getMaxHealth = onEntity getMaxHealth
--   setHealth = onEntityM .- setHealth


data Entity = forall e. (Show e, IsEntity e) => Entity e
deriving instance Show Entity

instance IsEntity Entity where
  getEntityImpl (Entity inner) = convertEntityImpl inner Entity
  getEntityType (Entity inner) = getEntityType inner
  getInner = id

convertEntityImpl :: (IsEntity a, Show a) => a -> (a -> b) -> EntityImpl b
convertEntityImpl inner cons = EntityImpl {
  getPosition' = getPosition' (getEntityImpl inner),
  setPosition' = \a b -> cons $ setPosition' (getEntityImpl inner) a b,
  getEntityId' = getEntityId' (getEntityImpl inner),
  getDirection' = getDirection' (getEntityImpl inner),
  getHealth' = getHealth' (getEntityImpl inner),
  getMaxHealth' = getMaxHealth' (getEntityImpl inner),
  setHealth' = \h -> cons $ setHealth' (getEntityImpl inner) h
  }

getPosition :: IsEntity a => a -> Pos
getPosition = getPosition' . getEntityImpl

setPosition :: IsEntity a => a -> Pos -> Direction -> a
setPosition = setPosition' . getEntityImpl

getEntityId :: IsEntity a => a -> Int
getEntityId = getEntityId' . getEntityImpl

getDirection :: IsEntity a => a -> Direction
getDirection = getDirection' . getEntityImpl

getHealth :: IsEntity a => a -> Int
getHealth = getHealth' . getEntityImpl

getMaxHealth :: IsEntity a => a -> Int
getMaxHealth = getMaxHealth' . getEntityImpl

setHealth :: IsEntity a => a -> Int -> a
setHealth = setHealth' . getEntityImpl

downcast :: forall e f. (IsEntity e, IsEntity f, Typeable f) => e -> Maybe f
downcast e = let out_tp = typeOf (undefined :: f)
                 real_tp = getEntityType e
              in if out_tp == real_tp then Just (case getInner e of
                       Entity a -> unsafeCoerce a) else Nothing
