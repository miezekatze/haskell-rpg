{-# LANGUAGE ExistentialQuantification, RankNTypes, StandaloneDeriving #-}
module GameClient(defaultState, gamePort,
                  emmitEvent, connectToServer,
                  runClientThread,
                  updatePendingState,
                  State(..),
                  Field(..),
                  renderEntity,
                  ClientEntity(getEntityInfo),
                  EntityInfo(EntityInfo, realPos),
                  withEntityAt, withEntityAt_,
                  getEntityAt, getYou,
                  EntityTable) where
import Network.Socket (PortNumber,
                       Socket,
                       HostAddress,
                       Family(AF_INET),
                       SocketType(Stream),
                       SockAddr (SockAddrInet),
                       SocketOption (ReuseAddr),
                       connect, socket, setSocketOption)
import Control.Concurrent (forkIO)
import Control.Concurrent.STM.TChan(TChan, isEmptyTChan)
import GameUtils
import Sdl (Sdl (getTextures), TextureType (TexturePlayer), sdlRenderTexture)
import Data.IORef (IORef, readIORef, writeIORef)
import Control.Concurrent.STM (atomically, readTChan)
import Control.Monad (unless, void)
import qualified Network.Socket.ByteString.Lazy as B
import Data.Binary (encode)
import Text.Printf (printf)
import qualified Data.Map as M(Map,insert,(!), {-fromList,-} (!?), empty)
import RandomFields (RandomSeed)
import Data.Maybe (fromJust)

emptyPlayer :: Pos -> ClientEntity
emptyPlayer pos = clientPlayer $ Player {
      getPlayerId = 0,
      playerPosition = pos,
      getPlayerDirection = DDown,
      getPlayerHealth = getMaxHealth (undefined :: Player),
      getPlayerName = "Player"
      }

defaultState :: Sdl -> IO State
defaultState sdl = do
  time <- getTime
  return State { getSdl = sdl,
                 lastFrameTime = time,
                 you = (0, 0),
                 getMap = M.empty,
                 zoom = 48.0,
                 oldYouPos = (0, 0),
                 getRandomSeed = fromIntegral time,
                 entityTable = M.empty,
                 ded = False
                 }

gamePort :: PortNumber
gamePort = 4169

emmitEvent :: GameEvent -> Socket -> IO ()
emmitEvent event sock = do
  case event of
    Connect -> void $ B.send sock $ encode eventConnect
    MoveRequest x y -> do
      void $ B.send sock $ encode eventMoveRequest
      void $ B.send sock $ encode x
      void $ B.send sock $ encode y
    Attack pid -> do
      void $ B.send sock $ encode eventAttack
      void $ B.send sock $ encode pid
    _ -> putStrLn $ "[err, emmitEvent] uninmplemented event: " ++ show event

connectToServer :: HostAddress -> IO Socket
connectToServer addr = do
  sock <- socket AF_INET Stream 0
  connect sock (SockAddrInet gamePort addr)
  emmitEvent Connect sock
  return sock

runClientThread :: HostAddress -> TChan GameEvent -> IO Socket
runClientThread addr chan = do
  sock <- connectToServer addr
  setSocketOption sock ReuseAddr 1
  _ <- forkIO $ handleGameEvent sock chan
  return sock

renderEntity :: IsEntity e => e -> Double -> Double -> Int -> Sdl -> IO ()
renderEntity e cx cy ss sdl = do
  let texIdx = getEntityId e
  let tex = getTextures sdl M.! (TexturePlayer texIdx $ getDirection e)
  
  let s = ss `div` 2
  sdlRenderTexture
    (round cx - fromIntegral s)
    (round $ cy - fromIntegral ss - fromIntegral s)
    ss (ss*2)
    (snd $ head tex) sdl

data EntityInfo = EntityInfo {
  realPos :: (Double, Double),
  endTime :: Time
  } deriving Show

data ClientEntity = ClientEntity {
  getEntityInfo :: EntityInfo,
  getClientEntity :: Entity
  }

-- onEntity :: (Entity -> t) -> ClientEntity -> t
-- onEntity g a = g (getEntityType a)

-- onEntityM :: (forall e. IsEntity e => e -> e) -> ClientEntity -> ClientEntity
-- onEntityM g a = a{getEntityType = g (getEntityType a)}

instance IsEntity ClientEntity where
    getEntityImpl ent = convertEntityImpl (getClientEntity ent) (\x -> ent {getClientEntity = x})
    getEntityType ent = getEntityType $ getClientEntity ent
    getInner = getClientEntity

instance Show ClientEntity where
  show ClientEntity{getEntityInfo = i, getClientEntity = t} =
    "{ renderEntity = <function>, getEntityInfo = " ++ show i
    ++ ", getEntityType = " ++ show t ++ " }"

data Object deriving Show

data Field = Field {
  getEntity :: Maybe Int,
  getType :: FieldType,
  getObject :: Maybe Object
  } deriving Show

clientPlayer :: Player -> ClientEntity
clientPlayer player = ClientEntity {
  getEntityInfo = EntityInfo { realPos = (0, 0), endTime = 0 },
  getClientEntity = Entity player
  }

type Map = M.Map Pos Field
type EntityTable = M.Map Int ClientEntity

data State = State {getSdl :: Sdl,
                    getRandomSeed :: RandomSeed,
                    lastFrameTime :: Time,
                    you :: Pos,
                    oldYouPos :: Pos,
                    getMap :: Map,
                    entityTable :: EntityTable,
                    zoom :: Double,
                    ded :: Bool
                    }
             deriving Show

updatePendingState :: IORef State -> TChan GameEvent -> IO ()
updatePendingState ioState chan = do
  let updatePendingState' ioState' chan' = do
        oldState <- readIORef ioState'
        isEmpty <- atomically (isEmptyTChan chan')
        unless isEmpty $ do
          event <- atomically $ readTChan chan'
          newState <- case event of
                YourPosition x y dt dir -> do
                  t <- getTime
                  printf "your position: (%d, %d)\n" x y
                  let newState = oldState { oldYouPos = you oldState,
                                      you = (fromIntegral x, fromIntegral y) }

                  let (px, py) = oldYouPos newState
                  let pid = case getEntity $ getMap oldState M.! (px, py) of
                        Just e -> e
                        v -> error $ "unreachable: " ++ show v

                  let oldPlayer = entityTable oldState M.! pid

                  let oldPlayer' = setPosition  oldPlayer (fromIntegral x, fromIntegral y) dir
                  let player = oldPlayer' { getEntityInfo = (getEntityInfo oldPlayer') { endTime = t + dt }}
                  let mp = getMap newState
                
                  let f =  mp M.! oldYouPos newState
                  let nf = f { getEntity = Nothing }
                  let mp' = M.insert (px, py) nf mp

                  let ix = fromIntegral x
                  let iy = fromIntegral y
                  
                  let f' =  mp' M.! (ix, iy)
                  let nf' = f' { getEntity = Just pid }
                  let nmp = M.insert (ix, iy) nf' mp'

                  return $ newState {getMap = nmp, you = (ix, iy), entityTable = M.insert
                                    pid player $
                                      entityTable oldState}
                ExploreField (x, y) ft -> do
--                  printf "explored field at (%d, %d): %s\n" x y (show ft)
                  let oldMap = getMap oldState
                  let oldField = oldMap M.!? (fromIntegral x, fromIntegral y)
                  let field = case oldField of
                        Just f -> f { getType = ft }
                        Nothing -> Field { getEntity = Nothing, getType = ft, getObject = Nothing }
                  let newMap = M.insert (fromIntegral x, fromIntegral y) field oldMap
                  return oldState { getMap = newMap }
                Health eid health -> do
                  let x = entityTable oldState M.! fromIntegral eid
                  let a = setHealth x (fromIntegral health)
                  let t' = M.insert (fromIntegral eid) a $ entityTable oldState
                  return oldState {entityTable = t'}
                Join pid x y -> do
                  let pos = (fromIntegral x, fromIntegral y)
                  let table' = M.insert (fromIntegral pid) (emptyPlayer pos) $ entityTable oldState
                  let m = getMap oldState
                  let f = m M.! pos
                  let f' = f { getEntity = Just (fromIntegral pid) }
                  let m' = M.insert pos f' m
                  
                  return oldState { entityTable = table', getMap = m' }
                Death eid -> do
                  if (fromIntegral eid == getEntityId (getYou oldState)) then do
                    putStrLn "YOU ARE DED!"
                    return oldState { ded = True }
                  else return oldState
                _ -> putStrLn ("[warn, updateState] uninmplemented event: " ++ show event) >> return oldState
          writeIORef ioState' newState
          updatePendingState' ioState' chan'
  updatePendingState' ioState chan

  nft <- getTime
  oldState <- readIORef ioState
  let newState = forEntities oldState nft interpolateMove
  let newState' = newState { lastFrameTime = nft }
--  print newState'
  writeIORef ioState newState'


interpolateMove :: State -> ClientEntity -> Time -> ClientEntity
interpolateMove state entity time =
  let lft = lastFrameTime state
      dft = time - lft

      info = getEntityInfo entity
      (rx, ry) = realPos info
      (x, y) = getPosition entity
      dx = fromIntegral x - rx
      dy = fromIntegral y - ry

      endTime' = endTime info
      timeLeft = endTime' - time

      relativeMove =  min 1 (fromIntegral dft / fromIntegral timeLeft)
      nx = if timeLeft < 0 then fromIntegral x else rx + dx * relativeMove
      ny = if timeLeft < 0 then fromIntegral y else ry + dy * relativeMove

      newInfo = info { realPos = (nx, ny) }
      newEntity = entity { getEntityInfo = newInfo } in
--      newState = state {you = newEntity} in
    newEntity

--  printf "rx = %f, x = %f, dx = %f (endTime = %d)\n" rx x dx endTime
--  printf "dft = %d, timeLeft = %d(%d, %d)\n" dft timeLeft endTime nft
--  printf "relative move: %f, absolute move (x): %f\n" relativeMove (dx * relativeMove)

--  writeIORef ioState newState

-- lastFrameTime = nft

forEntities :: State -> Time -> (State -> ClientEntity -> Time -> ClientEntity) -> State
forEntities state time f = 
  let t = entityTable state in
  let nt= fmap (\e -> f state e time) t in
  state { entityTable = nt }


withEntityAt :: Pos -> (ClientEntity -> (a, ClientEntity)) -> State -> (State, Maybe a)
withEntityAt p f s = case ent of
                       Just a -> let (val, ent') = f (entityTable s M.! a)
                                     table'      = M.insert a ent' $ entityTable s
                                     state'      = s { entityTable = table' }
                         in (state', Just val)
                       Nothing -> (s, Nothing)
  where
    mp = getMap s
    ent = do
      fl <- mp M.!? p
      e <- getEntity fl
      return e

withEntityAt_ :: Pos -> (ClientEntity -> a) -> State -> Maybe a
withEntityAt_ p f s = snd $ withEntityAt p (\x -> (f x, x)) s

getEntityAt :: Pos -> State -> Maybe ClientEntity
getEntityAt pos = withEntityAt_ pos id

getYou :: State -> ClientEntity
getYou state = fromJust $ getEntityAt (you state) state
