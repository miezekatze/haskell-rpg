{-# LANGUAGE LambdaCase #-}

module Render(renderState, handleEvent, initSdl, handleEvents, TextureType(..), Textures, Tex(..), changeYou) where

import System.Exit (exitFailure)
import GHC.Float (double2Int)
import qualified Data.Map as M(toList, (!), (!?))
import Control.Monad (when)
import Data.Int (Int32)
import Data.List (sortBy)
import Data.Function (on)

import GameClient
import Sdl
    ( Sdl(..),
      forEvents,

      initSdlInternal,
      renderSdl,
      sdlClear,

      sdlGetWindowSize,
      SdlEvent(SdlKeyboardEvent, SdlEventUnknown, SdlQuitEvent, sym),
      keycode,
      Textures,
      TextureType(..),
      Tex(..), sdlRenderTexture, sdlFillRect, red)
import GameUtils
import RandomFields (randomList, uniqueMatIndex)
import Data.Maybe (isJust)

initSdl :: Textures -> IO Sdl
initSdl images = do sdl' <- initSdlInternal images
                    case sdl' of
                      (Left s) -> do
                        putStrLn $ "Error initializing SDL: " ++ s
                        exitFailure
                      (Right a) -> return a

sdlRightArrow :: Int32
sdlRightArrow = 0x4000004f

sdlLeftArrow :: Int32
sdlLeftArrow = 0x40000050

sdlUpArrow :: Int32
sdlUpArrow = 0x40000052

sdlDownArrow :: Int32
sdlDownArrow = 0x40000051

changeYou :: (ClientEntity -> ClientEntity) -> State -> State
-- changeYou f state = let playerPos = you state
--                         playerField = getMap state M.! playerPos
--                         player = getEntity playerField
--                         map' = case player of
--                               Just e ->
--                                 let player' = f e
--                                     field' = playerField {getEntity = Just player'}
--                                 in M.insert playerPos field' $ getMap state
--                               _ -> undefined
--                         state' = state {getMap = map'}
--                     in state'
changeYou f s = fst $ withEntityAt (you s) (\x -> ((), f x)) s

handleEvent :: State -> SdlEvent -> IO (Bool, State, [GameEvent])
handleEvent state (SdlEventUnknown _x) = return (False, state, [])     -- don't exit
handleEvent state (SdlQuitEvent _) = return (True, state, []) -- just exit
handleEvent state e@SdlKeyboardEvent {} = do
  case keycode $ sym e of
    x
      | x == sdlRightArrow -> return (False, state, [MoveRequest 1 0])
      | x == sdlLeftArrow -> return (False, state, [MoveRequest (-1) 0])
      | x == sdlUpArrow -> return (False, state, [MoveRequest 0 (-1)])
      | x == sdlDownArrow -> return (False, state, [MoveRequest 0 1])
      | x == 0x68 {- 'h' -} -> return (False, state, [ Attack (fromIntegral $ getEntityId $ getYou state) ])
      | otherwise ->  return (False, state, [])

handleEvents :: State -> IO (Bool, State, [GameEvent])
handleEvents state = forEvents state handleEvent

listChooseRandom :: Bool -> Int -> [(Int, a)] -> a
listChooseRandom True _ xs = snd $ head xs
listChooseRandom False r xs' = xs !! (r `mod` length xs)
  where xs = xs' >>= uncurry replicate

renderField :: (Int, Int, Field) -> (Int, Int) -> Int -> State -> IO ()
renderField (x, y, f) (sx, sy) currentZoom state = do
  let sdl = getSdl state
  let randval = (randomList $ getRandomSeed state) !! uniqueMatIndex x y
  let field = getMap state M.! (x, y)
  let t =  listChooseRandom (isJust $ getObject field) randval $ getTextures sdl M.! TextureField (getType f)
  sdlRenderTexture sx sy currentZoom currentZoom t sdl
  -- case getType f of
  --   Grass -> sdlFillRect sx sy currentZoom currentZoom green sdl
  --   Stone -> sdlFillRect sx sy currentZoom currentZoom gray sdl

renderEntities :: Field -> (Int, Int) -> (Int, Int) -> Int -> Sdl -> EntityTable -> IO ()
renderEntities f (sx, sy) (x, y) currentZoom sdl tbl = do
  mapM_ (\e -> do
            let (rx, ry) = realPos $ getEntityInfo $ tbl M.! e
            let ox = fromIntegral sx + (rx - fromIntegral x) * fromIntegral currentZoom
            let oy = fromIntegral sy + (ry - fromIntegral y) * fromIntegral currentZoom
            renderEntity (tbl M.! e) (ox + fromIntegral currentZoom / 2)
              (oy + fromIntegral currentZoom / 2) currentZoom sdl
        ) $ getEntity f


renderUI :: State -> IO ()
renderUI state = do
  let playerPos = you state
  let playerField = getMap state M.! playerPos
  let player = getEntity playerField
  let tbl = entityTable state
  case player of
    Just e -> do
      let maxHealth = getMaxHealth (tbl M.! e)
      let health = getHealth (tbl M.! e)
      let relHealth = (fromIntegral health / fromIntegral maxHealth) :: Double
      sdlFillRect 10 10 (round $ relHealth * 200) 20 red $ getSdl state
    _ -> return ()

renderState :: State -> IO ()
renderState state = do
  let sdl = getSdl state
  sdlClear sdl
  (w,h) <- sdlGetWindowSize $ getWindow sdl

  let entity = case (getMap state M.!? you state) >>= getEntity of
            Just a -> a
            _ -> error "unreachable"

  let (EntityInfo { realPos = pos }) = getEntityInfo $ entityTable state M.! entity
  let currentZoom = zoom state
  let halfZoom = currentZoom / 2
  let center = pos

  let fields = M.toList $ getMap state

  let cpos x y = let dx = fromIntegral x - fst center
                     dy = fromIntegral y - snd center
                     sx = dx * currentZoom - halfZoom + fromIntegral (div w 2)
                     sy = dy * currentZoom - halfZoom + fromIntegral (div h 2)
                 in (sx, sy)

  let inImage sx sy = sx - halfZoom >= 0 && sx - halfZoom <= fromIntegral w
                   || sx + halfZoom >= 0 && sx + halfZoom <= fromIntegral w
                   || sy + halfZoom >= 0 && sy + halfZoom <= fromIntegral h
                   || sy + halfZoom >= 0 && sy + halfZoom <= fromIntegral h
  -- render fields
  mapM_ (\((x,y), f) -> do
            let (sx, sy) = cpos x y
            when (inImage sx sy) $
              renderField (x, y, f) (double2Int sx, double2Int sy) (double2Int currentZoom) state
        ) fields

  -- render entites
  mapM_ (\((x,y), f) -> do
            let (sx, sy) = cpos x y
            when (inImage sx sy) $
              renderEntities f (double2Int sx, double2Int sy) (x, y) (double2Int currentZoom) sdl $ entityTable state
        ) $ sortBy (compare `on` (snd . fst)) fields

  -- render UI
  renderUI state

  renderSdl $ getSdl state
